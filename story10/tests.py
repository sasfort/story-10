from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from .views import *
from .apps import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(Story10Config.name, 'story10')
        self.assertEqual(apps.get_app_config('story10').name, 'story10')

    def test_login_page_url_does_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_page_is_using_correct_function(self):
        found = resolve('/')
        self.assertEqual(found.func, login)

    def test_login_page_is_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')

    def test_welcome_page_url_redirects(self):
        response = Client().get('/welcome')
        self.assertEqual(response.status_code, 302)
    
    def test_welcome_page_is_using_correct_function(self):
        found = resolve('/welcome')
        self.assertEqual(found.func, welcome)

    def test_welcome_page_after_login(self):
        user = User.objects.create_user('uname', '', 'pword')
        self.client.login(username='uname', password='pword')
        response = self.client.get('/welcome')
        self.assertEqual(response.status_code,200)

    def test_welcome_using_index_template(self):
        user = User.objects.create_user('uname', '', 'pword')
        self.client.login(username='uname', password='pword')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn("Login", self.browser.title)
        self.assertIn("LOGIN", self.browser.page_source)