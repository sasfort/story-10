from django.shortcuts import render, redirect

# Create your views here.
def login(request):
    return render(request, 'login.html')

def welcome(request):
    return redirect('Story10:login')
