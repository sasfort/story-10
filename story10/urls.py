from django.urls import path
from .views import *

app_name = 'Story10'

urlpatterns = [
    path('', login, name='login'),
    path('welcome', welcome, name='welcome'),
]
